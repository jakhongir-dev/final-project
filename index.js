const preCalc = document.querySelector(".pre-calc");
const curCalc = document.querySelector(".cur-calc");
const btnContainer = document.querySelector(".btn-container");
const btnDark = document.querySelector(".theme-dark");
const btnLight = document.querySelector(".theme-light");

let haveDot = false;
const operationsArr = ["%", "/", "*", "-", "+"];

btnContainer.addEventListener("click", function (e) {
  const btn = e.target;
  // Numbers
  if (haveClass("btn-num")) {
    displayContent();
  } // Dot
  else if (haveClass("btn-dot")) {
    if (!haveDot) {
      displayContent();
    }
    haveDot = true;
  } // Clear
  else if (haveClass("btn-clear")) {
    curCalc.innerHTML = "";
    preCalc.innerHTML = "";
    haveDot = false;
  } // Operations
  else if (haveClass("btn-operation")) {
    haveDot = false;
    if (btn.innerHTML == "=") {
      curCalc.innerHTML = eval(curCalc.innerHTML).toFixed(2);
      preCalc.innerHTML = curCalc.innerHTML;
      curCalc.innerHTML = "";
    } else displayContent();
  }

  // Functions
  function haveClass(cls) {
    return btn.classList.contains(cls);
  }
  function displayContent() {
    curCalc.innerHTML += btn.textContent;
  }
});

btnDark.addEventListener("click", function (e) {
  themeHandler();
  e.target.classList.toggle("hidden");
  btnLight.classList.toggle("hidden");
});
btnLight.addEventListener("click", function (e) {
  themeHandler();
  e.target.classList.toggle("hidden");
  btnDark.classList.toggle("hidden");
});

function themeHandler() {
  document.body.classList.toggle("bodyBg");
  document.querySelector(".calc-box").classList.toggle("calcBox");
  document.querySelector(".btn-container").classList.toggle("btnContainer");
  document.querySelectorAll(".btn").forEach((el) => {
    el.classList.toggle("button");
  });
  document
    .querySelectorAll(".red")
    .forEach((el) => el.classList.toggle("red2"));
  document
    .querySelectorAll(".blue")
    .forEach((el) => el.classList.toggle("blue2"));
  document.querySelector(".pre-calc").classList.toggle("preCurCalc");
  document.querySelector(".cur-calc").classList.toggle("preCurCalc");
}
